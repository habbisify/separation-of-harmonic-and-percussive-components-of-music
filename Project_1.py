# -*- coding: utf-8 -*-
"""
Created on Thu Feb 21 13:10:25 2019

@authors: jonis, vrla

This is the project work for SGN-14007 2018-2019 Introduction to Audio Processing
Made in collaboration with Antti Vaarala

The assignment was to divide given music signal to its harmonic and percussive components
using the algorithm defined in the paper “Separation of a Monaural Audio Signal into
Harmonic/Percussive Components by Complementary Diffusion on Spectrogram”.
"""

#%%

import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from scipy.signal import stft, istft
from scipy.io import wavfile

#Read file and get the sample rate fs_org and the signal data f_org
fs_org,f_org = wavfile.read('rhythm_birdland.wav')

#%%

def showSpectrogram(*args):
    '''A function that plots spectrogram for the given signal
    
       args[0]:          figure index
       args[1], args[2]: signal, framerate (spectrogram); time, frequency (power spectrogram)
       args[3]:          title
       args[4]:          boolean (power spectrogram / not power spectrogram)
       args[5]:          signal in Fourier space (only if power spectrogram)
    '''
    
    plt.figure(args[0])
    
    if args[4]:
        plt.pcolormesh(args[1], args[2], 20*np.log10(args[5]))
    else:
        f_, t, S0 = signal.spectrogram(args[1], args[2])
        plt.pcolormesh(t, f_, 20*np.log10(S0))

    plt.ylabel('Frequency [Hz]')
    plt.xlabel('Time [sec]')
    plt.title(args[3])
    
    plt.show()

#%% 1 Calculate Ph,i, the STFT of an input signal f(t)

#Decimate the signal if needed; n_decimate = 1 for no decimation
n_decimate = 1
f = signal.decimate(f_org, n_decimate, ftype='iir')
fs = fs_org // n_decimate

#Define a non-symmetric Hann window with a reasonable window-size to be used in spectral analysis
winlen = 512
win = signal.hann(winlen, sym=False)

#Show the original signal for comparisons
showSpectrogram(0, f_org, fs_org, 'Spectrogram of original signal', False)

#Short Time Fourier Transform for the signal
#h: array of sample frequencies
#i: array of segment times
#F: STFT of f
h,i,F = stft(f, fs=fs, window=win, nperseg=winlen)

#%% 2 Calculate a range-compressed version of the power spectrogram

#Gamma is the separation parameter
#Use gamma value recommended in the paper (0.3)
gamma = 0.3

#W is the range-compressed version of the power spectrogram of the audio signal
W = np.abs(F)**(2*gamma)

#%% 3 Set initial values

#H is the harmonic component of the spectrogram
H = 0.5*W

#P is the percussive component of the spectrogram
P = 0.5*W

#%% 4 Calculate the update variables

#k is the kth derivative and increases with each iteration
k = 0
k_max = 50

while k < k_max:

    #Alpha controls the strength of the diffusion along the vertical and horizontal directions
    #Use alpha value recommended in the paper (0.3)
    alpha = 0.3
    
    #Initialize array of zeros to pad the missing values with zeros
    z_temp = np.zeros(H.shape)
    
    #initialize H(h, i +/- 1) values
    Hip = np.array(z_temp)
    Hip[:,:-1] += H[:,1:]
    Him = np.array(z_temp)
    Him[:,1:] += H[:,:-1]
    
    #Initialize P(h +/- 1, i) values
    Php = np.array(z_temp)
    Php[:-1,:] += H[1:,:]
    Phm = np.array(z_temp)
    Phm[1:,:] += H[:-1,:]
    
    #Calculate delta using formula (23)
    delta = alpha * ((Him - 2 * H + Hip) / 4) - (1 - alpha) * ((Phm - 2 * P + Php) / 4)
    
    #% 5 Update H and P
    
    #Calculate kth derivative of H and update H value with it
    #   kth derivative is defined as H + delta
    #   Take maximum element between kth derivative and 0 for each element
    #   Take minimum element between the maximum element and W
    H = np.minimum(np.maximum((H + delta),0), W)
    
    #P is the difference between W and H
    P = W - H
    
    #% 6 Increment k
    
    k += 1

#%% 7 Binarize the separation result

#Go through all values in H
#The result of last derivation for H and P gives either the power spectrogram element or 0
#If element of H is greater than the corresponding element of P, set H element equal to corresponding W element and make P element zero
#Else set P element equal to corresponding W element and make H element zero
for n,Hh in enumerate(H):
    
    for m in range(len(Hh)):
        
        if H[n][m] < P[n][m]:
            
            H[n][m] = 0
            P[n][m] = W[n][m]
        
        else:
            
            H[n][m] = W[n][m]
            P[n][m] = 0

#%% 8 Convert into waveforms

#Show the power spectrograms
showSpectrogram(1, i, h, 'Power spectrogram of original signal', True, W)
showSpectrogram(2, i, h, 'Power spectrogram of harmonic components', True, H)
showSpectrogram(3, i, h, 'Power spectrogram of percussive components', True, P)

#Transform H into waveform h
h = H ** (1 / (2 * gamma)) * np.exp(1j * np.angle(F))
_,h = istft(h,fs=fs,window=win,nperseg=winlen)

#Transform P into waveform p
p = P ** (1 / (2 * gamma)) * np.exp(1j * np.angle(F))
_,p = istft(p,fs=fs,window=win,nperseg=winlen)

#Show the spectrograms of the divided components
showSpectrogram(4, h, fs, 'Spectrogram of harmonic components', False)
showSpectrogram(5, p, fs, 'Spectrogram of percussive components', False)

#Create wav-files to the local directory
wavfile.write('h.wav', fs, np.int16(h))
wavfile.write('p.wav', fs, np.int16(p))

#Signal-to-noise ratio needs the original samples with only harmonic and percussive elements to work
#SNR = 10 * np.log10(f ** 2 / (h + p) ** 2)
#%%









