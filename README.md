This is my project work for SGN-14007 2018-2019 Introduction to Audio Processing.
Made in collaboration with Antti Vaarala.

The assignment was to divide given music signal to its harmonic and percussive components
using the algorithm defined in the paper “Separation of a Monaural Audio Signal into
Harmonic/Percussive Components by Complementary Diffusion on Spectrogram”.

This project demonstrates my ability to produce good quality code with Python and
familiarity with NumPy and SciPy. It also shows that I'm able to comprehend a scientific
paper and test its main content with a self-coded program.